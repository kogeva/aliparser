<?php

require 'vendor/autoload.php';

class DataSender
{
    const NODE_SERVER = 'http://localhost:3000';

    /**
     * @var \ElephantIO\Client
     */
    private static $sender;

    private function __construct() {}

    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$sender) {
            self::$sender = new \ElephantIO\Client(new \ElephantIO\Engine\SocketIO\Version1X(self::NODE_SERVER));
            self::$sender->initialize();
        }

        return self::$sender;
    }

    public static function emit($event, $data)
    {
        try {
            self::getInstance()->emit($event, $data);
        } catch (\ElephantIO\Exception\ServerConnectionFailureException $e) {
            printf("\033[0;31m %s \033[0m \n", $e->getMessage());
        }
    }

    public static function destroy()
    {
        self::getInstance()->close();
    }
}