<?php

require_once "../main-classes/autoload.php";

class KeyWordsSearcherNew
{

    const KEYWORD_URL_PATTERN = 'http://connectkeyword.aliexpress.com/lenoIframeJson.htm?__number=1&keyword=%s';

    public $keys;

    public function __construct($keys = [])
    {
        $this->keys = $keys;

        $this->keys = array_unique($this->keys);
    }

    public function searchKeywords($keys)
    {
        $urls = [];

        $set = [
            'info' => true,
            'auto_charset' => false
        ];

        foreach ($keys as $key) {
            $urls[] = [vsprintf(self::KEYWORD_URL_PATTERN, $key), $set];
        }

        $requests = \cURL::get($urls, $set);

        $tmp = [];

        foreach ($requests as $request) {

            if (!$request)
                throw new BadRequestException('Request is false');

            preg_match_all("/keywords: '([\\w\\s]+)'/", $request, $matches);

            $keyStorage = [];

            foreach ($matches[1] as $expression) {
                $newKeys = explode(' ', $expression);

                foreach ($newKeys as $k => $newKey) {
                    $keyStorage[] = $newKey;
                }
            }

            $keyStorage = array_unique($keyStorage);

            foreach ($keyStorage as $keyWord) {
                if (in_array($keyWord, $tmp)) {
                    unset($keyStorage[$k]);
                } else {
                    $tmp[] = $keyWord;
                }
            }
        }

        $this->keys = array_merge($this->keys, $tmp);

        $this->keys = array_unique($this->keys);

        return $this;
    }

    public function getKeys()
    {
        return $this->keys;
    }
}

$mongoDb = \DB\MongoDB::init();

$mongoDb = $mongoDb->selectDatabase('aliexpress');
$keywordCollection = $mongoDb->selectCollection('keywords');
$fullKeywordsCollection = $mongoDb->selectCollection('fullKeywords');
$cursor = $keywordCollection->find();

foreach ($cursor as $value) {
    $keywords = $value->data->exchangeArray();
}

$chunkLimit = 2;

$chunksArray = array_chunk($keywords, $chunkLimit);
$keywordSearcher = new KeyWordsSearcherNew();

$i = 0;
foreach ($chunksArray as $keysPart) {

    $keywordSearcher->searchKeywords($keysPart);
    sleep(1);

    $i++;

    $text = vprintf("Обработано: %s | Колличество ключей: %s \n", [
        (($i + 1) * $chunkLimit),
        count($keywordSearcher->getKeys())
    ]);

    echo $text;
}

$fullKeywordsCollection->insertOne(['data' => $keywordSearcher->getKeys()]);
