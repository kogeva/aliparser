<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 10.09.15
 * Time: 6:47
 */

/**
 * Class Product
 *
 * @method int getId()
 * @method string getName()
 * @method int getDiscount()
 * @method int getRatePercent()
 * @method int getCountOrders()
 * @method int getVotes()
 * @method float getPrice()
 * @method float getDiscountPrice()
 * @method int getUnit()
 * @method array getParams()
 * @method int getQuantity()
 * @method array getSpecifics()
 * @method string getDescription()
 * @method array getPackage()
 * @method array getImages()
 * @method Product setId(int $id)
 * @method Product setName(string $name)
 * @method Product setDiscount(int $discount)
 * @method Product setRatePercent(int $percent)
 * @method Product setCountOrders(int $countOrders)
 * @method Product setVotes(int $votes)
 * @method Product setPrice(float $price)
 * @method Product setDiscountPrice(float $discountPrice)
 * @method Product setUnit(int $unit)
 * @method Product setParams(array $params)
 * @method Product setQuantity(int $quantity)
 * @method Product setSpecifics(array $specifics)
 * @method Product setDescription(string $description)
 * @method Product setKeywords $setKeywords)
 * @method Product setPackage(array $packages)
 * @method Product setImages(array $images)
 */
class Product {

    public
        $keywords = null,
        $id = null,
        $name = false,
        $discount = false,
        $ratePercent = false,
        $countOrders = false,
        $votes = false,
        $price = false,
        $discountPrice = false,
        $unit = false,
        $params = false,
        $quantity = false,
        $specifics = [],
        $description = false,
        $package = false,
        $images = [];

    function __construct($id = null)
    {
        $this->id = $id;
    }

    function __call($name, $arguments)
    {
        $accessor = substr($name, 0, 3);
        $attr = lcfirst(substr($name, 3, strlen($name)));

        if($accessor == 'set') {
            if(gettype($arguments[0]) == 'string' && !strlen($arguments[0]))
                return null;
            else
                $this->$attr = $arguments[0];
        }
        elseif($accessor == 'get')
            return $this->$attr;
        else
            throw new Exception(sprintf('Attribute %s not fount in class %s', [$attr, __CLASS__]));

        return $this;
    }

    /**
     * @return array $this
     */
    public function toArray() {
        return (array) $this;
    }
} 