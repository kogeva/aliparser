<?php

require "autoload.php";

//Lock::check();
//require "DataSender.php";
//require "KeyWordsExceptions/BadRequestException.php";
//require "KeyWordsExceptions/KeysNotMatchedException.php";

/**
 * Парсер кейвордов
 *
 * Class KeyWordsSearcher
 */
class KeyWordsSearcher
{
    const KEYWORD_URL_PATTERN = 'http://connectkeyword.aliexpress.com/lenoIframeJson.htm?__number=1&keyword=%s';

    const KEYS_COLLECTION_NAME = 'keys';

    const SEARCHED_KEYS_COLLECTION_NAME = 'searchedKeys';

    const SKIPPED_KEYS_COLLECTION_NAME = 'skippedKeys';

    public $keys = [];

    public $searchedKeys = [];

    public $skippedKeys = [];

    private $database;

    private $logFile;

    public function __construct(MongoDB\Database $database)
    {
        $this->database = $database;

        $this->fillKeys(self::KEYS_COLLECTION_NAME);
        $this->fillKeys(self::SEARCHED_KEYS_COLLECTION_NAME);
        $this->fillKeys(self::SKIPPED_KEYS_COLLECTION_NAME);

        $filePath = __DIR__ . '/log/result.log';

        $this->logFile = fopen($filePath, 'w');
    }

    public function searchKeywords($keys, $chunkLimit, $level = 1)
    {
        Lock::check();

        $keysPoint = $this->keys;
        $searchedKeysPoint = $this->searchedKeys;
        $skippedPoint = $this->skippedKeys;

        sleep(1);
        $urls = [];

        $keys = $this->filterCurrentKeys($keys);

        foreach ($keys as $key) {
            if (self::fastInArray($key, $this->searchedKeys))
                return false;

            $set = [
                'info' => true,
                'auto_charset' => false,
                'proxy' => Proxy::getElite()
            ];

            $urls[] = [vsprintf(self::KEYWORD_URL_PATTERN, urlencode($key)), $set];
        }

        $requests = \cURL::get($urls, $set);

        $tmp = [];

        foreach ($requests as $k => $request) {
            if (!$request) {
                $this->skippedKeys[] = $keys[$k];
                continue;
            }

            preg_match_all("/keywords: '([\\w\\s]+)'/", $request, $matches);

            if (!isset($matches[1]) && count($matches[1]) === 0)
                continue;

            $keyStorage = $combinationOfWord = [];

            foreach ($matches[1] as $expression) {
                $strKeyword = mb_strtolower($expression);
                $combinationOfWord[] = $strKeyword;
                $newKeys = explode(' ', $strKeyword);

                $keyStorage = array_merge($newKeys, $combinationOfWord);
            }

            $keyStorage = array_unique($keyStorage);

            foreach ($keyStorage as $k => $keyWord) {
                if (in_array($keyWord, $tmp)) {
                    unset($keyStorage[$k]);
                } else {
                    $tmp[] = $keyWord;
                }
            }

            $this->searchedKeys[] = $key;
        }

        $this->keys = array_merge($tmp, $this->keys);

        $this->searchedKeys = array_unique($this->searchedKeys);
        $this->keys = array_unique($this->keys);
        $this->skippedKeys = array_unique($this->skippedKeys);

        $diffKeys = array_diff($this->keys, $keysPoint);
        $diffSearchedKeys = array_diff($this->searchedKeys, $searchedKeysPoint);
        $skippedKeys = array_diff($this->skippedKeys, $skippedPoint);

        $this->addKeysToCollection(self::KEYS_COLLECTION_NAME, $diffKeys);
        $this->addKeysToCollection(self::SEARCHED_KEYS_COLLECTION_NAME, $diffSearchedKeys);
        $this->addKeysToCollection(self::SKIPPED_KEYS_COLLECTION_NAME, $skippedKeys);

        $countBadRequest = count($this->skippedKeys);
        $foundKeys = count($tmp);
        $totalKeys = count($this->keys);
        $searchedKeys = count($this->searchedKeys);

        $currentDateTime= date("c");

        $message = "{$currentDateTime} | Уровень рекурсии: {$level} | Не обработанных: {$countBadRequest} | ";
        $message .= "Найденно: {$foundKeys} | Всего: {$totalKeys} | Обработанно: {$searchedKeys} \n";

        $this->writeLog($message);
        Lock::setMessage("Обр-но({$searchedKeys})");

        foreach ($tmp as $k => $v) {
            if (in_array($v, $keys))
                unset($tmp[$k]);
        }

        $chunks = array_chunk($tmp, $chunkLimit);

        $nextLevel = $level + 1;

        foreach ($chunks as $chunk) {
            $this->searchKeywords($chunk, $chunkLimit, $nextLevel);
        }
    }

    /**
     *
     * @param array $keys
     * @param string $fieldName
     * @return $this
     */
    private function addKeysToCollection($fieldName, $keys)
    {
        $preparedKeys = [];

        foreach ($keys as $key) {
            $preparedKeys[] = ['key' => $key];
        }

        if (count($preparedKeys) > 0) {
            $this->database->{$fieldName}->insertMany($preparedKeys);
        }

        return $this;
    }

    private function fillKeys($collectionName)
    {
        $collection = $this->database->$collectionName;
        $cursor = $collection->find();

        foreach ($cursor as $item) {
            $this->{$collectionName}[] = $item['key'];
        }
    }

    /**
     * @param string $needle
     * @param array $haystack
     * @return bool
     */
    public static function fastInArray($needle, $haystack)
    {
        $array = array_flip($haystack);

        return (isset($array[$needle])) ? true : false;
    }

    public function getKeys()
    {
        return $this->keys;
    }

    private function writeLog($string)
    {
        fwrite($this->logFile, $string);
    }

    public function filterCurrentKeys($currentKeys)
    {
        foreach ($this->keys as $key) {
            foreach ($currentKeys as $k => $v) {
                if ($key == $v) {
                    unset($currentKeys[$v]);
                }
            }
        }

        return array_values($currentKeys);
    }
}

$mongoDb = \DB\MongoDB::getInstance();

$productCollection = $mongoDb->aliexpress->offers;
$productCursor = $productCollection->find(['key_parsed' => false]);

$keywordSearcher = new KeyWordsSearcher($mongoDb->aliexpress);
$chunkLimit = 50;

$products = [];

foreach ($productCursor as $product) {
    $products[] = $product;
}

foreach ($products as $product) {
    $nameWords = explode(' ', $product->name);

    $chunks = array_chunk($nameWords, $chunkLimit);

    $i = 0;
    foreach ($chunks as $chunk) {
        try {
            $keywordSearcher->searchKeywords($chunk, $chunkLimit);
        } catch (BadRequestException $e) {
            echo "$e \n";
        } catch (KeysNotMatchedException $e) {
            echo "$e \n";
        }

        $i++;

        $text = vprintf("Обработано: %s | Колличество ключей: %s \n", [
            (($i + 1) * $chunkLimit),
            count($keywordSearcher->getKeys())
        ]);

        echo $text;
    }

    $productCollection->updateOne(
        ['id' => $product->id],
        ['$set' => ['key_parsed' => true]]
    );
}