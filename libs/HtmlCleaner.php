<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 11.03.15
 * Time: 22:48
 */

class HtmlCleaner {

    static function clean($html)
    {
        $tags = [
            '</div>',
            '</address>',
            '</center>',
            '</blockquote>',
            '</fieldset>',
            '</form>',
            '</h1>',
            '</h2>',
            '</h3>',
            '</h4>',
            '</h5>',
            '</h6>',
            '</hr>',
            '</isindex>',
            '</menu>',
            '</ol>',
            '</ul>',
            '</p>',
            '</pre>',
            '</table>',
            '<br>',
            '<br/>',
            '<br />'
        ];

        $pattern = '/\<\/?(?:img|div|address|center|blockquote|fieldset|form|h\d|hr|isindex|menu|ol|ul|p|pre|table|(?:br\s*\/*))\>/i';
        $prepareText = preg_replace($pattern, '*[endLine]*', $html);

        $prepareText = strip_tags($prepareText);
        $prepareText = self::htmlChars($prepareText);
        $prepareText = mb_convert_encoding ($prepareText, 'HTML-ENTITIES');
        $prepareText = str_replace(["\r", "\t", "\n"], ' ', $prepareText);

        $p = ['/&#?[^;]+;/', '/ {2,}/'];
        $r = ['', ' '];

        $prepareText = preg_replace($p, $r, $prepareText);
        $prepareText = str_replace('*[endLine]*', "<br>", $prepareText);

        $p = ['/\s*([\<\>])\s*/', '/(?:\<br\>){3,}/', '/(?:^(?:\<br\>)+|(?:\<br\>)+$)/'];
        $r = ['\\1', '<br><br>', ''];

        $prepareText = preg_replace($p, $r, $prepareText);

        return trim($prepareText);
    }

    /**
     * Функционал htmlspecialchars из 5.4
     * ENT_HTML5
     *
     * @param string $str
     * @param bool $decode
     * @return string
     */
    static function htmlChars ($str, $decode = false) {
        $replace = array (
            '&apos;' => "'",
            '&quot;' => '"',
            '&amp;' => '&',
            '&laquo;' => '"',
            '&raquo;' => '"',
            '&#187;' => '"',
            '&#171;' => '"',
            '&#8217;' => "'",
            '&#8216;' => "'",
            '&#8221;' => '"',
            '&#8220;' => '"',
            '&#8222;' => '"',
            '&#34;' => "'",
            '&#39;' => '"',
            '&nbsp;' => ' '
        );

        $str = strtr ($str, $replace);
        return $str;
    }
} 