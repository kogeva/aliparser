<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 09.09.15
 * Time: 14:33
 */

require_once 'libs/phpQuery-onefile.php';
require_once 'models/Product.php';
require_once 'libs/CTTR/autoload.php';

/**
 * Class AliParser
 *
 */
class AliParserEn
{
    const PRODUCT_URL_PATTERN = "http://www.aliexpress.com/item/test/%s.html?site=en";
    const PRODUCT_DESCRIPTION_URL_PATTERN = 'http://m.aliexpress.com/item-desc/%s.html?site=en';

    public $notFoundIds = [];

    public function parse($ids)
    {
        $results = $this->multiRequest($ids, self::PRODUCT_URL_PATTERN);
//        $resultsContent = $this->multiRequest($ids, self::PRODUCT_DESCRIPTION_URL_PATTERN);

        $productCollection = [];

        foreach ($results as $k => $v) {
//            $productCollection[$k] = $this->convertFromHtml($k, $v, $resultsContent[$k]);
            try {
                $productCollection[$k] = $this->convertFromHtml($k, $v);
            } catch (Exception $e) {
                $this->notFoundIds[] = "$k";
            }
        }

        return $productCollection;
    }

    /**
     * @param int $id
     * @param string $mainHtml
     * @param string $contentHtml
     * @return Product
     */
    public function convertFromHtml($id, $mainHtml, $contentHtml = null)
    {
        $product = new Product($id);

//        $phpQueryContent = phpQuery::newDocument($contentHtml);
        $phpQuery = phpQuery::newDocumentHTML($mainHtml);

        if ($phpQuery->find('#magnet-404-hot')->get(0) !== null)
            throw new Exception('404');

        $keywords = $phpQuery->find('meta[name="keywords"]')->get(0);

        if($keywords)
            $keywords = $keywords->getAttribute('content');

        $description = $phpQuery->find('meta[name="description"]')->get(0)->getAttribute('content');

        /** NAME */
        $name = $phpQuery->find('.product-name')->text();

        /** COUNT ORDERS */
        $countOrders = $phpQuery->find('.orders-count b')->text();

        /** COUNT VOTES */
        $votes = $phpQuery->find('.product-star')->text();

        /** PRICE */
        $price = str_replace(['US ', '$'], '', $phpQuery->find('span[itemprop="price"]')->text());

        if(!$price) {
            $lowPrice = $phpQuery->find('span[itemprop="lowPrice"]')->text();
            $highPrice = $phpQuery->find('span[itemprop="highPrice"]')->text();

            $price = ['lowPrice' => $lowPrice, 'highPrice' => $highPrice];
        }

        /** DISCOUNT PRICE */
        $discountPrice = str_replace(' ', '', $phpQuery->find('#sku-discount-price')->text());

        /** UNIT */
        preg_match("/([a-z]+)/",  $phpQuery->find('#product-price')->text(), $match);
        $unit = $match[1];

        /** QUANTITY */
        $quantity = $phpQuery->find('#quantity-no')->text();

        /** SKU */
        $params = $phpQuery->find('#product-info-sku dl');

        /** RATING */
        $rating = $phpQuery->find('.product-star b')->text();

        /** PROPERTIES */
        $specifications = $phpQuery->find('.ui-attr-list.util-clearfix');

        /** PACKAGE */
        $package = $phpQuery->find('.pnl-packaging .ui-attr-list.util-clearfix');

        /** PACKAGE */
        $images = $phpQuery->find('.image-nav-item img');

        /** DESCRIPTION */
//        $description = $phpQueryContent->find('.descriptions')->html();

        $specificationsArray = $paramsArray = $imgArray = $packageArray = [];

        $product->setName($name);
        $product->setCountOrders($countOrders);
        $product->setUnit($unit);
        $product->setPrice($price);
        $product->setDiscountPrice($discountPrice);
        $product->setRatePercent($rating);
        $product->setDescription($description);
        $product->setKeywords($keywords);

        if (strlen($discountPrice))
            $product->setDiscount(100 - round($discountPrice * 100 / $price));

        if (strlen($quantity)) $product->setQuantity(preg_replace('/[^\d]+/', '', $quantity));
        if (strlen($votes)) {
            preg_match('/\((\d+) votes\)/', $votes, $matches);

            if(isset($matches[1]))
                $product->setVotes($matches[1]);
        }

        if ($images->count()) {
            foreach ($images as $image) {
                $imgArray[] = substr($image->getAttribute('src'), 0, -10);
            }
            $product->setImages($imgArray);
        }

        if ($specifications->count()) {
            foreach ($specifications as $row) {
                $key = str_replace(':', '', $row->getElementsByTagName('dt')->item(0)->textContent);
                $val = $row->getElementsByTagName('dd')->item(0)->textContent;

                $specificationsArray[$key] = $val;
            }

            $product->setSpecifics($specificationsArray);
        }

        if ($params->count()) {
            foreach ($params as $elem) {
                $label = str_replace(':', '', trim($elem->getElementsByTagName('dt')->item(0)->textContent));
                $listValues = $elem->getElementsByTagName('li');

                $labelValues = [];
                $imgFlag = false;

                foreach ($listValues as $value) {
                    $img = $value->getElementsByTagName('img');

                    if ($img->item(0)) {
                        $imgFlag = true;
                        $labelValues[] = $img->item(0)->getAttribute('src');
                    } else
                        $labelValues[] = $value->textContent;

                    $img = null;
                }

                $paramsArray[$label] = ['type' => ($imgFlag) ? 'img' : 'text', 'data' => $labelValues];
            }

            $product->setParams($paramsArray);
        }

        if ($package->count()) {
            foreach ($package as $row) {
                $key = str_replace(':', '', $row->getElementsByTagName('dt')->item(0)->textContent);
                $val = $row->getElementsByTagName('dd')->item(0)->textContent;
                $packageArray[$key] = $val;
            }

            $product->setPackage($packageArray);
        }

        return $product;
    }

    /**
     * @param array $ids
     * @param string $urlPattern
     * @return mixed
     */
    function multiRequest($ids, $urlPattern)
    {
        $workers = [];
//        $proxiesData = Proxy::getEliteAll(count($ids));

        $proxyIterator = 0;

        foreach ($ids as $id) {
            $set = [
                'info' => true,
                'autocharset' => false,
                'cookiefile' => '/tmp/cookies.txt'
//                'proxy' => $proxiesData[$proxyIterator++]
            ];

            $workers[$id] = [vsprintf($urlPattern, [$id]), $set];
        }

        return cURL::get($workers);
    }
} 